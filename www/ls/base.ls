ig = window.ig

init = ->
  ciselnik = {}
  ciselnik_arr = []
  for line in ig.data.ciselnik.split "\n"
    [kod, nazev] = line.split "\t"
    continue if kod == "kod"
    continue unless kod
    ciselnik[kod] = nazev
    nazevSearchable = nazev.toLowerCase!stripDiacritics!
    ciselnik_arr.push {kod, nazev, nazevSearchable}
  new Tooltip!watchElements!
  container = d3.select ig.containers.base
  impExpGraph = new ig.ImpExpGraph container, ciselnik
  if -1 != window.location.hash.indexOf 'us-export'
    impExpGraph.drawUs!
  else
    impExpGraph.drawRu!
  header = new ig.Header container, impExpGraph, ciselnik


if d3?
  init!
else
  $ window .bind \load ->
    if d3?
      init!
