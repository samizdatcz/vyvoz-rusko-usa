ig.utils = utils = {}
ig.existing =
  "ru-export": <[0 01 017 0172 0173 0174 0175 0176 0179 02 022 0221 0222 0223 0224 023 024 0242 0243 0249 025 0251 0252 03 034 0341 035 0353 037 0371 0372 04 041 0412 042 0422 043 045 0451 046 0461 0462 047 0471 0472 048 0481 0482 0483 0484 0485 05 054 0542 0545 0546 0547 0548 056 0561 0564 0566 0567 057 0571 0572 0573 0575 0576 0577 0579 058 0581 0582 0583 0589 059 0591 0592 0593 0599 06 061 0612 0616 0619 062 0621 0622 07 071 0712 0713 072 0721 0722 073 0731 0732 0733 0739 074 0741 075 0751 0752 09 091 0910 098 0981 0984 0985 0986 0989 1 11 111 1110 112 1121 1122 1123 1124]>
  "ru-exportAssoc": {}
  "us-export": <[0 01 017 0172 0173 0174 0175 0176 0179 02 022 0221 0222 0223 0224 023 024 0242 0243 0249 025 0251 03 034 0341 035 0353 037 0371 0372 04 041 0412 042 0422 043 046 0461 0462 047 0472 048 0481 0482 0483 0484 0485 05 054 0542 0545 0546 0548 056 0561 0564 0566 0567 057 0571 0572 0575 0576 0577 0579 058 0581 0583 0589 059 0591 0592 0593 0599 06 061 0612 0616 0619 062 0621 0622 07 071 0712 0713 072 0721 0722 073 0731 0732 0733 0739 074 0741 075 0751 0752 09 091 0910 098 0981 0984 0985 0986 0989 1 11 111 1110 112 1121 1122 1123 1124]>
  "us-exportAssoc": {}

for itm in ig.existing."ru-export"
  ig.existing."ru-exportAssoc"[itm] = true
for itm in ig.existing."us-export"
  ig.existing."us-exportAssoc"[itm] = true
utils.offset = (element, side) ->
  top = 0
  left = 0
  do
    top += element.offsetTop
    left += element.offsetLeft
  while element = element.offsetParent
  {top, left}


utils.deminifyData = (minified) ->
  out = for row in minified.data
    row_out = {}
    for column, index in minified.columns
      row_out[column] = row[index]
    for column, indices of minified.indices
      row_out[column] = indices[row_out[column]]
    row_out
  out


utils.formatNumber = (input, decimalPoints = 0) ->
  input = parseFloat input
  if decimalPoints
    wholePart = Math.floor input
    decimalPart = input % 1
    wholePart = insertThousandSeparator wholePart
    decimalPart = Math.round decimalPart * Math.pow 10, decimalPoints
    decimalPart = decimalPart.toString()
    while decimalPart.length < decimalPoints
      decimalPart = "0" + decimalPart
    "#{wholePart},#{decimalPart}"
  else
    wholePart = Math.round input
    insertThousandSeparator wholePart


insertThousandSeparator = (input, separator = ' ') ->
    price = Math.round(input).toString()
    out = []
    len = price.length
    for i in [0 til len]
      out.unshift price[len - i - 1]
      isLast = i is len - 1
      isThirdNumeral = 2 is i % 3
      if isThirdNumeral and not isLast
        out.unshift separator
    out.join ''

utils.backbutton = (parent) ->
  parent.append \a
    ..attr \class \backbutton
    ..html '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" baseProfile="full" width="76" height="76" viewBox="0 0 76.00 76.00" enable-background="new 0 0 76.00 76.00" xml:space="preserve"><path fill="#000000" fill-opacity="1" stroke-width="0.2" stroke-linejoin="round" d="M 57,42L 57,34L 32.25,34L 42.25,24L 31.75,24L 17.75,38L 31.75,52L 42.25,52L 32.25,42L 57,42 Z "/></svg>'
